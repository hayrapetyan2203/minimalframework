<?php

/**
 * Autoloader Class
 *
 * This class is responsible for autoloading classes based on their namespace and location.
 */
class Autoloader
{
    /**
     * The namespace prefix
     *
     * @var string
     */
    private $prefix;

    /**
     * The base directory for the namespace prefix
     *
     * @var string
     */
    private $base_dir;

    /**
     * Autoloader constructor
     *
     * @param string $prefix The namespace prefix
     * @param string $base_dir The base directory for the namespace prefix
     */
    public function __construct($prefix, $base_dir)
    {
        $this->prefix = $prefix;
        $this->base_dir = $base_dir;
    }

    /**
     * Register the autoloader function
     *
     * Registers the loadClass method with spl_autoload_register
     */
    public function register()
    {
        spl_autoload_register(array($this, 'loadClass'));
    }

    /**
     * Autoloader function
     *
     * This function is called by spl_autoload_register and is responsible for loading the
     * class files.
     *
     * @param string $class The name of the class to be loaded
     */
    public function loadClass($class)
    {
        // does the class use the namespace prefix?
        $len = strlen($this->prefix);
        if (strncmp($this->prefix, $class, $len) !== 0) {
            // no, move to the next registered autoloader
            return;
        }

        // get the relative class name
        $relative_class = substr($class, $len);

        // replace the namespace prefix with the base directory, replace namespace
        // separators with directory separators in the relative class name, append
        // with .php
        $file = $this->base_dir . str_replace('\\', '/', $relative_class) . '.php';

        // if the file exists, require it
        if (file_exists($file)) {
            require $file;
        }
    }
}

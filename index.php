	<?php
# This is the entry point of the program
# All the necessary imports and initializations should be done here

# Autoload necessary classes
# Use a PSR-4 compliant autoloader

# Load configuration files
# These files contain the necessary configuration values for the application to run

# Set error handler
# Use a custom error handler or PHP's default error handler to handle errors and exceptions

# Run front controller
# The front controller is the main point of entry for all HTTP requests
# It routes the request to the appropriate controller and executes the necessary action


// Load the autoloader file for classes
require_once('vendor/autoloader.php');

// Create an autoloader instance for the 'Minimal\' namespace
$autoloader = new Autoloader('Minimal\\', __DIR__ . '/src/Minimal/');

// Register the autoloader
$autoloader->register();

// Create an autoloader instance for the 'Controllers\' namespace
$autoloader = new Autoloader('Controllers\\', __DIR__ . '/Controllers/');

// Register the autoloader
$autoloader->register();

// Create an autoloader instance for the 'Models\' namespace
$autoloader = new Autoloader('Models\\', __DIR__ . '/Models/');

// Register the autoloader
$autoloader->register();

// Create an instance of the error handler class
$errorHandler = new Minimal\ErrorHandler();

// Load the 'development.php' configuration file
Minimal\Config::load('development.php');

// Create an instance of the front controller
$front_controller = new Minimal\FrontController();

// Run the front controller
$front_controller->run();

// End of script

?>

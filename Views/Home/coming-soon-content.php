<?php echo \Minimal\AssetLoader::loadCSS('Home/content.css'); ?>

<!-- The main content -->
<div class="container mt-5 d-flex align-items-center justify-content-center">
  <!-- Wrapper div for the welcome message and buttons -->
  <div>
    <!-- Welcome message with big font size -->
    <h1 class="text-center display-1">Comig Soon</h1>
    <!-- Lead text for the framework -->
    <?php $this->render('content/tagline'); ?>
    <!-- Button group for different sections of the website -->
    <?php $this->render('content/buttonGroup'); ?>
  </div>
</div>
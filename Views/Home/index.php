<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Include Bootstrap CSS -->
    <?php echo \Minimal\CDN::includeBootstrapCSS(); ?>
    
    <title>Hello, Minimnal!</title>
  </head>
  <body>
    <!-- The render method in this line is calling the content view and rendering it -->
    <?php $this->render('content'); ?>

    <!-- The render method in this line is calling the footer view and rendering it -->
    <?php $this->render('footer'); ?>

    <!-- Include jQuery -->
    <?php echo \Minimal\CDN::includeJQuery(); ?>
    <!-- Include Vue -->
    <?php echo \Minimal\CDN::includeVue(); ?>
  </body>
</html>



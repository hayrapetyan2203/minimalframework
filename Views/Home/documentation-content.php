<?php echo \Minimal\CDN::includeFontAwesome(); ?>
<!-- Include Font Awesome CSS -->
<div class="container-fluid p-5">
  <div class="row">
    <div class="col-md-2 bg-light py-5 sidebar">
      <h4 class="text-center mt-3">Terms of use</h4>
      <hr class="bg-dark w-25">
      <ul class="nav flex-column">
        <li class="nav-item">
          <a class="nav-link text-dark" href="/Home/Documentation/action/Instalation">
            <i class="fa fa-cog mr-2"></i>Instalation
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-dark" href="/Home/Documentation/action/Models">
            <i class="fa fa-cog mr-2"></i>Models
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-dark" href="/Home/Documentation/action/Views">
            <i class="fa fa-cog mr-2"></i>Views
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-dark" href="/Home/Documentation/action/Controllers">
            <i class="fa fa-cog mr-2"></i>Controllers
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-dark" href="/Home/Documentation/action/AssetLoading">
            <i class="fa fa-cog mr-2"></i>Asset Loading
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-dark" href="/Home/Documentation/action/MinimalAPI">
            <i class="fa fa-cog mr-2"></i>Minimal API
          </a>
        </li>
      </ul>
    </div>
    <div class="col-md-10">
      <?php 
        switch ($action['action']) {
          case 'Instalation':
            $this->render('documentation/Instalation'); 
            break;          
          case 'Models':
            $this->render('documentation/Models'); 
            break;          
          case 'Views':
            $this->render('documentation/Views'); 
            break;          
          case 'Controllers':
            $this->render('documentation/Controllers'); 
            break;          
          case 'AssetLoading':
            $this->render('documentation/AssetLoading'); 
            break;          
          case 'MinimalAPI':
            $this->render('documentation/MinimalAPI'); 
            break;          
          case 'FAQ':
            $this->render('documentation/FAQ'); 
            break;
          default:
            $this->render('documentation/index'); 
            break;
        }
      ?>
    </div>
  </div>
</div>

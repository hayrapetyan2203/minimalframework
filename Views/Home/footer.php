<!-- Footer -->
<footer class="bg-light py-3 fixed-bottom">
  <div class="container">
    <p class="text-center">Copyright &copy; Powered by Minimal - The simple solution for efficient web development 2023</p>
  </div>
</footer>

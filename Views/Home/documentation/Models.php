    <div class="container my-5">
      <h1 class="text-center">Models</h1>
      <hr />
      <h2>Introduction</h2>
      <p>
        Models are abstract representations of complex systems or phenomena. They allow us to simplify reality and make predictions based on simplified inputs. In the realm of data science and machine learning, models are algorithms that can learn from data and make predictions about new, unseen data. These models can range from simple linear models to complex deep neural networks. The choice of model depends on the specific problem being solved and the available data. By creating and training models, we can gain valuable insights into the relationships between inputs and outputs and make informed decisions.

      <p>The base model class is the foundation for all models in a given framework or library. It provides a standardized interface and common functionality that can be reused across multiple models. This allows developers to focus on creating new models without having to worry about writing low-level code. The base model class may include methods for training, evaluation, prediction, and serialization. By building on top of the base model class, developers can quickly create new models with less effort, resulting in faster development times and more consistent code across projects.</p>
      </p>
      <h2>Features</h2>
      <ul>
        <li>CRUD:</li>
        <li>Execute custom SQL:</li>
      </ul>
      <h2>Usage</h2>
      <p>
        This code is a PHP class called BaseModel that provides common database functions such as connecting to the database, CRUD (Create, Read, Update, Delete) operations, and error handling. To use this code, you need to have PHP and a database system installed and configured. To use the class, you need to extend it in your model class and use its functions in your PHP application. The database connection is established using the \Minimal\Database class and the PDO (PHP Data Objects) extension.
        <p>In Minimal Framework you can just use our BaseModel, but if you want to have your own models - all you have to do is to create a model iside of the `Models` folder and <strong>dont forget to include Models namespace to it.</strong></p>
      </p>
      <h2>Examples</h2>
      <ol>
      	<li>
      		Insert a new record into the database:
			<pre><p class="bg-dark text-success rounded">

			# Declare an array of data to be inserted into the database table
			$data = [
			    'field1' => 'value1',
			    'field2' => 'value2',
			    'field3' => 'value3',
			];

			# Create a new instance of the BaseModel class
			$model = new \Models\BaseModel();

			# Insert the data into the database table
			$model->create('table_name', $data);

			</pre></p>
      	</li>      	
      	<li>
      		Retrieve all records from a table:
			<pre><p class="bg-dark text-success rounded">
			# Create instance of BaseModel
			$model = new \Models\BaseModel();

			# Get all results from table_name
			$results = $model->read('table_name');

			# Iterate over each result
			foreach ($results as $result) {
			    # Perform some action with each result
			}

			</pre></p>
      	</li>      	
      	<li>
      		Update a record in the database:
			<pre><p class="bg-dark text-success rounded">
			# Define an array with new values to update
			$data = [
			    'field1' => 'new_value1',
			    'field2' => 'new_value2',
			];

			# Instantiate the BaseModel
			$model = new \Models\BaseModel();

			# Update the table with the new values
			$model->update('table_name', $data, $id);

			</pre></p>
      	</li>      	
      	<li>
      		Delete a record from the database:
			<pre><p class="bg-dark text-success rounded">
			# Create an instance of the BaseModel class
			$model = new \Models\BaseModel();

			# Delete a record in the table "table_name" with the specified id
			$model->delete('table_name', $id);

			</pre></p>
      	</li>      	
      <li>
      		Execute a custom sql query:
			<pre><p class="bg-dark text-success rounded">
			# Usage example of the executeSQL method from BaseModel class
			$model = new \Models\BaseModel();

			# Define a custom SQL command to select all data from table_name
			$sql = "SELECT * FROM table_name";

			# Execute the SQL command
			$results = $model->executeSQL($sql);

			# Loop through the results
			foreach ($results as $result) {
			    # Do something with each result
			}
			</pre></p>
      	</li>
      </ol>
      <h2>Conclusion</h2>
      <p>
        In conclusion, models are an essential tool for understanding and making predictions about complex systems and data. By using a base model class, developers can streamline the process of creating new models and reduce the amount of redundant code. The models documentation page has provided an overview of the purpose and benefits of models, as well as the key features of the base model class. With this information, developers can quickly get up and running with creating new models and making data-driven predictions. Whether you're a seasoned data scientist or just starting out, the power of models is something that should not be overlooked.
      </p>
    </div>

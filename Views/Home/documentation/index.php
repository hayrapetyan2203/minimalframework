<div class="jumbotron text-center">
  <h1 class="display-4">Welcome to the Documentation</h1>
  <p class="lead">Here you will find all the information you need to get started with our framework.</p>
</div>

<div class="container mt-5">
  <p class="lead text-center">Our product documentation is designed to help you get the most out of our product. Whether you are a first-time user or a seasoned pro, this documentation will provide you with all the information you need to make the most of our product.</p>
  <div class="row mt-5">
    <div class="col-lg-4">
      <h3 class="text-center mb-3"><a href="/Home/Documentation/action/Instalation" class="text-dark">Getting Started</a></h3>
      <p>Learn how to set up your product, how to use its core features, and how to get started using it.</p>
    </div>
    <div class="col-lg-4">
      <h3 class="text-center mb-3"><a href="/Home/Documentation/action/MinimalAPI" class="text-dark">API Guide</a></h3>
      <p>A comprehensive reference guide to all the features and functionality of our product.</p>
    </div>
    <div class="col-lg-4">
      <h3 class="text-center mb-3"><a href="/Home/Documentation/action/FAQ" class="text-dark">FAQs</a></h3>
      <p>Get answers to the most commonly asked questions about our product.</p>
    </div>
  </div>
</div>

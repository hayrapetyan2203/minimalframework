<div class="container p-5">
  <h1 class="text-center mb-5">Installation</h1>
  <h2>Composer Installation</h2>
  <p>To install this package using composer, run the following command:</p>
  <pre class="alert alert-danger p-3 rounded">
composer require MinimalFramework/v1
  </pre>
  <h2>GitHub Installation</h2>
  <p>If you prefer to install using GitHub, follow these steps:</p>
  <ol>
    <li>
      <p>Clone the repository: with HTTPS</p>
      <pre class="alert alert-danger p-3 rounded">
          git clone https://gitlab.com/hayrapetyan2203/minimalframework.git
      </pre>
      <hr>
      <p>Clone the repository: with SSH</p>
      <pre class="alert alert-danger p-3 rounded">
          git@gitlab.com:hayrapetyan2203/minimalframework.git
      </pre>
    </li>
  </ol>
  <p class="text-center mt-5">
    That's it! You're now ready to start using this package in your project.
  </p>
</div>

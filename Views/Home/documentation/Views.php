    <div class="container my-5">
      <h1 class="text-center">Views</h1>
      <hr />
      <h2>Introduction</h2>
      <p>
        In web development, Views refer to the user-facing pages or templates that present data to the end-user. They are responsible for rendering the data received from the controller, combining it with the appropriate HTML, CSS and JavaScript, and presenting it to the user in an aesthetically pleasing manner. The View component in MVC (Model-View-Controller) architecture is responsible for the presentation of data. It is important to keep the Views separated from the data processing and business logic, to allow for easy maintenance and scaling. In this documentation, we will explore the concepts and implementation of Views in web development, including best practices and examples.
      </p>
      <h2>Features</h2>
      <ul>
        <li>Data Presentation:</li>
        <li>Dynamic Content:</li>
        <li>Decoupling:</li>
        <li>Responsiveness:</li>
      </ul>
      <h2>Usage</h2>
      <p>
        <p>In Minimal Framework to create a view all you need is to create a folder that have <strong>the same name as the controller</strong> without the `controller` postfix</p>
        <p>You can have any counts of subfolders and views inside of your view folder.</p>
        <code>For example` if you in `Controllers` folder have a controller named LogInController: Then you have to create a folder in `View` folder and name it LogIn</code>
      </p>
     
      <h2>Conclusion</h2>
      <p>
        In conclusion, Views is a powerful tool that can simplify the process of presenting data in your application. It separates the logic of data presentation from the underlying model, making it easier to maintain and change the display of your data. With its many features and flexible API, Views allows developers to create custom, dynamic and engaging user interfaces with ease. Whether you are building a simple website or a complex web application, Views is a valuable tool that can help you achieve your goals.
      </p>
    </div>

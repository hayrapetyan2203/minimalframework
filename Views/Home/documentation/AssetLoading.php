    <div class="container my-5">
      <h1 class="text-center">Asset Loading</h1>
      <hr />
      <h2>Introduction</h2>
      <p>
        The process of asset loading refers to the method of including external resources such as stylesheets, scripts, and images in a web application. These assets play a crucial role in enhancing the user experience and presentation of a web application. The introduction of asset loading in a web application is important to ensure the efficient and proper functioning of the application. In this docpage, we will cover the basics of asset loading and how it can be integrated into a web application for optimal results.</p>
      <h2>Features</h2>
      <ul>
        <li>Easy maintenance and organization of stylesheets and scripts through centralization of assets:</li>
        <li>Use of versioning to ensure that clients receive the latest version of assets and cache issues are avoided:</li>
        <li>Improved website performance by reducing the number of HTTP requests needed to load a page:</li>
      </ul>
      <h2>Usage</h2>
      <p>
        We are using simple asset loader, which can work load css,js and img files for you.
        <p>In Minimal Framework you can store your assets in `css`, `js` and `img` folders and then you can simly call our asset loader class's static methods to load them</p>
        <p><code>For example` you can use \Minimal\AssetLoader::loadCSS('file name') to load a css file</code></p>
        <p><code>For example` you can use \Minimal\AssetLoader::loadJS('file name') to load a js file</code></p>
        <p><code>For example` you can use \Minimal\AssetLoader::loadImage('Image name') to load a Image</code></p>
      </p>
     
      <h2>Conclusion</h2>
      <p>
        In conclusion, our asset loader is a simple and strong class that can make your development way to easier.
      </p>
    </div>

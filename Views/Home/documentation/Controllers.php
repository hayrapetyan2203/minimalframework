    <div class="container my-5">
      <h1 class="text-center">Controllers</h1>
      <hr />
      <h2>Introduction</h2>
      <p>
        Controllers play a vital role in the Model-View-Controller (MVC) architecture pattern. They act as a mediator between the Model, which handles data and logic, and the View, which displays information to the user. In a web application, Controllers receive user input, process it, and determine how to respond. The Controller acts as the glue that binds the Model and View together. This documentation will provide an overview of Controllers and their functions, including how to create and implement them in your web application.</p>
      <h2>Features</h2>
      <ul>
        <li>Handling user input:</li>
        <li>Coordinating Model and View:</li>
        <li>Implementing business logic:</li>
        <li>Routing:</li>
        <li>Security:</li>
        <li>Reusability:</li>
      </ul>
      <h2>Usage</h2>
      <p>
        Controllers are the essential part of a MVC architecture that control the flow of data between models and views. They receive user input and based on that they process the data, perform business logic and provide the response back to the views to be displayed to the end user. To use a controller, you need to create a class that extends the base controller class and implement its methods to handle specific actions such as displaying a list of items or processing a form submission. The methods in the controller should call the relevant methods from the model to interact with the data and then pass the data to the view for rendering.
        <p>In Minimal Framework if you want to create a controller all you have to do is to create a controller iside of the `Controllers` folder and <strong>dont forget to include Controllers namespace to it.</strong></p>
      </p>
      <h2>Examples</h2>
      <ol>
      	<li>
      		display the index view.
			<pre><p class="bg-dark text-success rounded">
			    /**
			     * Index function is the main entry point for the home page.
			     * It calls the render function to display the index view.
			     */
			    public function Index()
			    {
			        $this->render('index');
			    }
			</pre></p>
      	</li>      	
      	<li>
      		display the documentation view, take an get parameter and pass it like to the view as a variable.
			<pre><p class="bg-dark text-success rounded">
			    /**
			     * Documentation function is the main entry point for the home page.
			     * It calls the render function to display the Documentation view.
			     */
			    public function Documentation($action)
			    {
			        $this->render('documentation', ['action' => $action]);
			    }
			</pre></p>
      	</li>      	
      	<li>
      		display the documentation view, take data from model and to the view.
			<pre><p class="bg-dark text-success rounded">
			    /**
			     * Documentation function is the main entry point for the home page.
			     * It calls the render function to display the Documentation view.
			     */
			    public function Documentation()
			    {
			    	$model = new \Models\DocumentationModel();
			    	$action = $model->getDocumentation();
			        $this->render('documentation', ['action' => $action]);
			    }
			</pre></p>
      	</li>      	
      </ol>
      <h2>Conclusion</h2>
      <p>
        In conclusion, controllers play a crucial role in the MVC design pattern by serving as the intermediary between the model and the view. They process user inputs, retrieve data from the model, and trigger updates to the view. By centralizing this logic, controllers help to maintain a clear separation of concerns and improve the overall organization and maintainability of the application. It is important to note that the design of controllers can greatly impact the performance, scalability, and testability of an application, so it is crucial to make well-informed design decisions.
      </p>
    </div>

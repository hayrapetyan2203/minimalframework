<?php 

namespace Minimal;

/**
 * Class BootstrapCDN
 * 
 * This class provides a simple way to include the latest version of Bootstrap framework and jQuery library.
 * It provides two methods, one for including the CSS files and another for including the JS files.
 */
class CDN
{
    /**
     * This function outputs the link to the Bootstrap CSS file.
     *
     * @return string The link to the Bootstrap CSS file
     */
    public static function includeBootstrapCSS()
    {
        return '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">';
    }

    /**
     * This function outputs the link to the jQuery JavaScript file.
     *
     * @return string The link to the jQuery JavaScript file
     */
    public static function includeJQuery()
    {
        return '<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha384-ZvpUoO/+PpLXR1lu4jmpXWu80pZlYUAfxl5NsBMWOEPSjUn/6Z/hRTt8+pR6L4N2" crossorigin="anonymous"></script>';
    }

    /**
     * This function outputs the link to the Vue JavaScript file.
     *
     * @return string The link to the Vue JavaScript file
     */
    public static function includeVue()
    {
        return '<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>';
    }    

    /**
     * This function outputs the link to the FontAwesome file.
     *
     * @return string The link to the FontAwesome file
     */
    public static function includeFontAwesome()
    {
        return '<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">';
    }
}


<?php 
namespace Minimal;

/**
*
* REST API Abstraction Layer Class
* This class provides an additional layer of abstraction for making REST API requests, making it easier and more efficient to interact with APIs.
* The class uses cURL to make API requests, and provides helper functions for common HTTP methods such as GET, POST, PUT and DELETE.
* The makeRequest() function can be used as a helper function to make cURL requests, allowing for custom API requests to be made as needed.
*/

class RESTClient {

    // Base URL for API requests
    private $baseUrl;

    // HTTP headers to be sent with each request
    private $headers;

    // Constructor to set base URL and default headers
    public function __construct($baseUrl, $headers = []) 
    {
        $this->baseUrl = $baseUrl;
        $this->headers = $headers;
    }

    public function get($endpoint, $queryParams = []) 
    {
        // Build the URL for the GET request
        $url = $this->baseUrl . $endpoint;

        // Add query parameters to the URL, if any
        if (!empty($queryParams)) {
            $url .= '?' . http_build_query($queryParams);
        }

        // Make the GET request
        $response = $this->makeRequest('GET', $url);

        // Return the response
        return $response;
    }


    public function post($endpoint, $data = [])
     {
        // Build the URL for the POST request
        $url = $this->baseUrl . $endpoint;

        // Make the POST request
        $response = $this->makeRequest('POST', $url, $data);

        // Return the response
        return $response;
    }


    public function put($endpoint, $data = []) 
    {
        // Build the URL for the PUT request
        $url = $this->baseUrl . $endpoint;

        // Make the PUT request
        $response = $this->makeRequest('PUT', $url, $data);

        // Return the response
        return $response;
    }

    public function delete($endpoint, $data = []) 
    {
        // Build the URL for the DELETE request
        $url = $this->baseUrl . $endpoint;

        // Make the DELETE request
        $response = $this->makeRequest('DELETE', $url, $data);

        // Return the response
        return $response;
    }


    private function makeRequest($method, $endpoint, $data = []) 
    {
        // Initialize the cURL session
        $ch = curl_init();

        // Set the options for the cURL session
        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

        // If there is data to send in the request
        if (!empty($data)) {
            // Set the data to be sent in the request
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        }

        // Execute the cURL session
        $response = curl_exec($ch);

        // Close the cURL session
        curl_close($ch);

        // Return the response
        return $response;
    }

}


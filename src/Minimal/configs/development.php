<?php
/**
 * Configuration settings for the application
 *
 * This file contains all the configuration settings for the application.
 * It returns an associative array with the config keys and values.
 *
 * @package    Database Access Controll System
 * @author     Armen Hayrapetyan
 * @copyright  Copyright (c) 2023, Armen Hayrapetyan
 * @license    https://opensource.org/licenses/MIT  MIT License
 * @link       https://example.com
 * @since      1.0.0
 */

return [
    'db_host' => 'localhost',
    'db_name' => 'mysql',
    'db_username' => 'root',
    'db_password' => '',
    'app_url' => 'http://localhost/dba_admin',
    'env' => 'development'
];

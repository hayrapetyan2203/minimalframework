<?php 

namespace Minimal;

use Exception;

/**
 * Config class
 * 
 * This class is used to handle the global configuration for the application
 * It uses an array to store the config key-value pairs
 * It has two method set and get to store and retrieve the config values respectively
 */

class Config
{
    private static $config = array();
    

    const CONFIG_PATH = __DIR__ . '/configs/';

    /**
     * Load config values from a file
     * 
     * @param string $file_name The name of the config file
     * @throws Exception If the config file is not found or is not readable
     */
    public static function load($file_name) {
        $file_path = self::CONFIG_PATH . $file_name;
        if (!is_readable($file_path)) {
            throw new Exception("Config file not found or not readable");
        }
        $config = include $file_path;
        self::$config = array_merge(self::$config, $config);
    }

    /**
     * method to add config value
     * @param string $key
     * @param mixed $value
     */
    public static function set($key, $value)
    {
        self::$config[$key] = $value;
    }
    
    /**
     * method to get config value
     * @param string $key
     * @return mixed
     */
    public static function get($key)
    {
        if (isset(self::$config[$key])) {
            return self::$config[$key];
        }
        return null;
    }
}

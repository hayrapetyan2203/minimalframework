<?php

namespace Minimal;

class OutputHelper {

  /**
   * Outputs a success response in JSON format.
   * 
   * @param mixed $data Data to be included in the response
   * @param int $status HTTP status code
   */
  public static function success($data, $status = 200) {
    http_response_code($status);
    header('Content-Type: application/json');
    echo json_encode(['success' => true, 'data' => $data]);
  }

  /**
   * Outputs an error response in JSON format.
   * 
   * @param string $message Error message
   * @param int $status HTTP status code
   */
  public static function error($message, $status = 400) {
    http_response_code($status);
    header('Content-Type: application/json');
    echo json_encode(['success' => false, 'error' => $message]);
  }


  /**
   * Function to display a variable in pre tags
   *
   * @param mixed $var The variable to be displayed
   *
   * @return void
   */
  public static function pre($var)
  {
      echo "<pre>";
      print_r($var);
      echo "</pre>";
  }

}

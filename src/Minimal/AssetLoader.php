<?php 

namespace Minimal;

/**
 * AssetLoader class provides functions to load CSS, JavaScript, and image files
 */
class AssetLoader {
  /**
   * Function to load CSS file
   *
   * @param string $file Path to CSS file
   * @return string HTML string to include CSS file
   */
  public static function loadCSS($file) {
    return '<link rel="stylesheet" type="text/css" href="/css/'. $file .'">';
  }

  /**
   * Function to load JavaScript file
   *
   * @param string $file Path to JavaScript file
   * @return string HTML string to include JavaScript file
   */
  public static function loadJS($file) {
    return '<script type="text/javascript" src="/js/'. $file .'"></script>';
  }

  /**
   * Function to load image file
   *
   * @param string $file Path to image file
   * @param string $alt Alternative text for image
   * @return string HTML string to include image file
   */
  public static function loadImage($file, $alt = '') {
    return '<img src="/img/'. $file .'" alt="'. $alt .'">';
  }
}
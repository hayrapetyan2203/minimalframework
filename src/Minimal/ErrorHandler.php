<?php
namespace Minimal;

/**
 * Class ErrorHandler
 * This class is used to handle all types of errors and exceptions that may occur in the application.
 * It registers a shutdown function and an error handler that are called when a fatal error or any type of error occurs.
 *
 * The handleShutdown method is called when the script execution finishes, whether it finishes normally or with a fatal error. 
 * This method checks if there was a fatal error and calls the handleFatalError method to handle it.
 *
 * The handleError method is called when a non-fatal error occurs, such as an E_WARNING or E_NOTICE. 
 * This method can be used to log non-fatal errors and display a custom error message to the user.
 *
 * The handleFatalError method is called when a fatal error occurs, such as an E_ERROR or E_PARSE. 
 * This method can be used to log the fatal error and display a custom error message to the user.
 */
class ErrorHandler
{
    public function __construct()
    {
        register_shutdown_function([$this, 'handleShutdown']);
        set_error_handler([$this, 'handleError']);
        set_exception_handler([$this, 'handleException']);
    }

    /**
     * This method is called when the script execution finishes, whether it finishes normally or with a fatal error.
     * It checks if there was a fatal error and calls the handleFatalError method to handle it.
     */
    public function handleShutdown()
    {
        $error = error_get_last();
        if ($error && ($error['type'] === E_ERROR || $error['type'] === E_PARSE || $error['type'] === E_COMPILE_ERROR)) {
            $this->handleFatalError($error);
        }
    }

    /**
     * This method is called when a non-fatal error occurs, such as an E_WARNING or E_NOTICE.
     * It can be used to log non-fatal errors and display a custom error message to the user.
     *
     * @param int $errno
     * @param string $errstr
     * @param string $errfile
     * @param int $errline
     */
    public function handleError($errno, $errstr, $errfile, $errline)
    {
        print_r($errno);
        print_r($errstr);
        print_r($errfile);
        print_r($errline);
        // Log non-fatal error
    }

    /**
     * This method is called when a fatal error occurs, such as an E_ERROR or E_PARSE.
     * It can be used to log the fatal error and display a custom error message to the user.
     *
     * @param array $error
     */
    public function handleFatalError($error)
    {
        // Log fatal error
        ErrorLogger::logError('An error has occurred' . print_r($error));

        echo '<div class="container">';
        echo '<div class="alert alert-danger">';
        echo '<h4 class="alert-heading">Fatal Error</h4>';
        echo '<p>' . $error['message'] . '</p>';
        echo '<hr>';
        echo '<p class="mb-0">File: ' . $error['file'] . '</p>';
        echo '<p class="mb-0">Line: ' . $error['line'] . '</p>';
        echo '</div>';
        echo '</div>';
    }


        /**
     * Custom exception handler function
     *
     * This function is used to handle uncaught exceptions. It is specified as the 
     * exception handler using the set_exception_handler() function.
     *
     * @param Throwable $exception The exception object that was thrown
     *
     * @return void
     */
    public function handleException($exception) 
    {
        // Log the error to a file
        ErrorLogger::logError('An error has occurred' . $exception);
        print_r($exception->getMessage() . ' in ' . $exception->getFile() . ' on line ' . $exception->getLine());

        // Show a message to the user
        echo 'An error occurred. We apologize for the inconvenience.';
    }

}

<?php
namespace Minimal;
/**
 * FrontController
 *
 * This class acts as a front controller for a web application. It captures the current URI from the server and uses it to determine the controller, action, and parameters to be passed to the Route class for processing.
 */
class FrontController
{
    /**
     * @var array $uri_parts An array containing the parts of the current URI
     */
    public $uri_parts;

    /**
     * Constructor
     *
     * Initializes the $uri_parts property by calling the parseUri() method and passing in the REQUEST_URI from the server.
     */
    public function __construct()
    {
        $this->uri_parts = $this->parseUri($_SERVER['REQUEST_URI']);
    }

    /**
     * run
     *
     * Instantiates a new Route object and calls its match() method, passing in the controller, action, and parameters.
     */
    public function run()
    {
        $route = new Route();
        $route->match($this->uri_parts['controller'], $this->uri_parts['action'], $this->uri_parts['params']);
    }

    /**
     * parseUri
     *
     * Parses the current URI and returns an array containing the controller, action, and parameters.
     *
     * @param string $uri The current URI being requested
     *
     * @return array An array containing the parts of the URI
     */
    protected function parseUri($uri)
    {
        // Remove leading and trailing slashes from the URI
        $uri = trim($uri, '/');

        // Split the URI into an array
        $parts = explode('/', $uri);

        // Set the controller and action, defaulting to 'home' if not present
        $controller = (isset($parts[0]) && !empty($parts[0])) ? $parts[0] : 'home';
        $action = (isset($parts[1]) && !empty($parts[1])) ? $parts[1] : 'index';

        // Get the remaining parts of the URI as parameters
        $params = array();
        for($i=2;$i<count($parts);$i+=2)
        {
            $params[$parts[$i]] = $parts[$i+1];
        }

        return array(
            'controller' => $controller,
            'action' => $action,
            'params' => $params
        );
    }
}

<?php
namespace Minimal;

use Exception;

class Route
{
    /**
     * This function matches the URI to the corresponding controller and action.
     * It also passes the params as an argument to the action method.
     * 
     * @param string $controller The name of the controller to be matched
     * @param string $action The name of the action to be matched
     * @param array $params The parameters passed in the URI
     * 
     * @throws Exception If the controller file, class or action method is not found
     */
    public function match($controller, $action, $params)
    {
        $controller = ucfirst($controller) . 'Controller';
        $file = 'Controllers/' . $controller . '.php';
        $controller = 'Controllers\\' . $controller;
        // Check if the controller file exists
        if (file_exists($file)) {
            require_once $file;
            
            // Check if the controller class and action method exist
            if (class_exists($controller) && method_exists($controller, $action)) {
                $controller = new $controller;
                $controller->$action($params);
            } else {
                throw new Exception("Class or method not found");
            }
        } else {
            throw new Exception("File not found");
        }
    }
}

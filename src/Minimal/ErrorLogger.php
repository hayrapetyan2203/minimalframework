<?php
namespace Minimal;

class ErrorLogger
{
    public static function logError($error, $filePath = './Logs/', $maxFileSize = 100000)
    {
        try {
            // Get the current date and time to use in the file name
            $dateTime = date('Y-m-d_H-i-s');

            // Build the file name
            $fileName = $filePath . "log_{$dateTime}.txt";

            // Check if the file exists
            if (file_exists($fileName)) {
                // Get the file size
                $fileSize = filesize($fileName);

                // Check if the file size is greater than the maximum file size
                if ($fileSize > $maxFileSize) {
                    // Build the new file name
                    $fileName = $filePath . "log_{$dateTime}_1.txt";
                }
            }

            // Open the file for writing
            $file = fopen($fileName, 'a');

            // Write the error to the file
            fwrite($file, $error . PHP_EOL);

            // Close the file
            fclose($file);
        } catch (Exception $e) {
            // Log the exception message
            error_log('Error: ' . $e->getMessage());
        }
    }
}


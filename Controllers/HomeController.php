<?php

namespace Controllers;

/**
 * HomeController class is used to handle the logic for the home page of the website.
 * It extends BaseController class to inherit common functionality.
 */
class HomeController extends BaseController
{
    /**
     * Index function is the main entry point for the home page.
     * It calls the render function to display the index view.
     */
    public function Index()
    {
        $this->render('index');
    }

    /**
     * Documentation function is the main entry point for the home page.
     * It calls the render function to display the Documentation view.
     */
    public function Documentation($action)
    {
        $this->render('documentation', ['action' => $action]);
    }    

    /**
     * Forum function is the main entry point for the home page.
     * It calls the render function to display the Forum view.
     */
    public function Forum()
    {
        $this->render('forum');
    }    

    /**
     * donate function is the main entry point for the home page.
     * It calls the render function to display the donate view.
     */
    public function Donate()
    {
        $this->render('donate');
    }
}

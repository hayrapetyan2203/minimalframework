<?php 
namespace Controllers;

use ReflectionClass;

/**
 * Base Controller class 
 *
 * This class contains common functionality for all controllers
 * in the application such as render() function
 */
class BaseController {

    /**
     * @var array $data Data to be passed to the view
     */
    protected $data = array();

    /**
     * BaseController constructor.
     *
     * Initialize default data
     */
    public function __construct() 
    {
        $this->setDefaultData();
    }
    
    /**
     * Set default data for the class
     */
    public function setDefaultData() {
        $this->data['title'] = 'Default Title';
        $this->data['body'] = 'Default Body';
    }

    /**
     * Render a view file
     *
     * @param string $view The name of the view file (without .php extension)
     * @param array $data Data to be passed to the view
     *
     * @return void
     */
    protected function render($view, $data = [], $fileType = '.php') {
        // Extract data from array to individual variables
        extract($data);
        // include the view file
        require_once 'Views/'. $this->getClassName() . '/' . $view . $fileType;
    }

    /**
     * Get the class name without namespace and without 'Controller'
     *
     * @return string
     */
    protected function getClassName() {
        $class = new ReflectionClass($this);
        return str_replace("Controller", "", $class->getShortName());
    }

    /**
     * Main function that processes the incoming POST data in a secure way
     *
     * @return void
     */
    function getPostData()
    {
        // Check if the request method is POST
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            // Sanitize the POST data
            $data = sanitizePostData();

            // Check if the data is not empty
            if (!empty($data)) {

                // Call the protected function to process the POST data
                processData($data);
            }
        }
    }

    /**
     * Sanitizes the incoming POST data
     *
     * @return array The sanitized POST data
     */
    function sanitizePostData()
    {
        return filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    }

    /**
     * Process the POST data in a secure way
     *
     * @param array $data The sanitized POST data
     *
     * @return void
     */
    protected function processData(array $data)
    {
        return $data;
    }


    /**
     * Encodes data for safe display in a view
     *
     * @param string $data The data to encode
     *
     * @return string The encoded data
     */
    public function encode($data)
    {
        return htmlentities($data, ENT_QUOTES, 'UTF-8');
    }

}

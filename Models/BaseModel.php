<?php 

namespace Models;

use PDO; 

/**
 * BaseModel class serves as the foundation for all other model classes in the project. 
 * It provides common functionality such as database connectivity, CRUD operations, and error handling. 
 * This class should be extended by all other model classes to ensure consistent behavior throughout the application.
 */
class BaseModel {
    protected $db;

    public function __construct() {
        //initialize the database connection here
        $pdo = \Minimal\Database::getInstance();
        $this->db = $pdo->getConnection();
    }
    
    /**
     * Insert a new record into the database
     *
     * @param string $table The name of the table to insert into
     * @param array $data The data to insert into the table
     *
     * @return bool
     */
    public function create($table, $data) {
        try {
          $keys = array_keys($data);

          $query = "INSERT INTO $table (" . implode(', ', $keys) . ") VALUES (" . implode(', ', array_map(function($v) { return '?'; }, $keys)) . ")";
          $stmt = $this->db->prepare($query);
          $i = 1;
          foreach($data as $key => $value) {
              $stmt->bindValue($i, $value);
              $i++;
          }

          return $stmt->execute();
        } catch (PDOException $e) {
          // Handle errors
          echo "Error: " . $e->getMessage();
        }
    }

    /**
     * Retrieve all records from a table
     *
     * @param string $table The name of the table to retrieve data from
     *
     * @return array
     */
    public function read($table) {
        try {
          $query = "SELECT * FROM $table";
          $stmt = $this->db->prepare($query);
          $stmt->execute();
          return $stmt->fetchAll();
        } catch (PDOException $e) {
          // Handle errors
          echo "Error: " . $e->getMessage();
        }
    }
    
    /**
     * Update a record in the database
     *
     * @param string $table The name of the table to update
     * @param array $data The new data for the record
     * @param int $id The ID of the record to update
     *
     * @return bool
     */
    public function update($table, $data, $id) {

        try {
          $primaryKey = $this->getPrimaryKey($table);
          $query = "UPDATE $table SET";
          foreach ($data as $key => $value) {
              $query .= " $key = ?,";
          }
          $query = rtrim($query, ',');
          $query .= " WHERE $primaryKey = ?";
          $stmt = $this->db->prepare($query);
          $i = 1;
          foreach($data as $key => $value) {
              $stmt->bindValue($i, $value);
              $i++;
          }
          $stmt->bindValue($i, $id);
          return $stmt->execute();
        } catch (PDOException $e) {
          // Handle errors
          echo "Error: " . $e->getMessage();
        }
    }

    /**
     * Delete a record from the database
     *
     * @param string $table The name of the table to delete from
     * @param int $id The ID of the record to delete
     *
     * @return bool
     */
    public function delete($table, $id) {
        try {
          $primaryKey = $this->getPrimaryKey($table);
          $query = "DELETE FROM $table WHERE $primaryKey = :id";
          $stmt = $this->db->prepare($query);
          $stmt->bindValue(':id', $id);
          return $stmt->execute();
        } catch (PDOException $e) {
          // Handle errors
          echo "Error: " . $e->getMessage();
        }
    }

    /**
     * Function to get the primary key of a table
     *
     * @param string $table - name of the table
     * @return string $primaryKey - name of the primary key
     */
    public function getPrimaryKey($table) {
        try {
          // Prepare query to show keys for given table
          $stmt = $this->db->prepare("SHOW KEYS FROM $table WHERE Key_name = 'PRIMARY'");
          $stmt->execute();
          // Fetch result
          $result = $stmt->fetch(PDO::FETCH_ASSOC);
          // Store primary key column name
          $primaryKey = $result['Column_name'];
          return $primaryKey;
        } catch (PDOException $e) {
          // Handle errors
          echo "Error: " . $e->getMessage();
        }
    }

    /**
     * Execute a custom SQL command
     *
     * @param string $sql The SQL command to execute
     * @return mixed Result of the SQL command
     */
    public function executeSQL($sql) {
        try {
          // Prepare and execute the SQL command
          $stmt = $this->db->prepare($sql);
          $stmt->execute();

          // Return the result of the SQL command
          return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
          // Handle errors
          echo "Error: " . $e->getMessage();
        }
    }

}

